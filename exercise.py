class World:
    def __init__(self, dimension):
        self.world = [[i] for i in xrange(dimension)]

    def print_world(self):
        for i, stack in enumerate(self.world):
            pos = str(i) + ":"
            for block in stack:
                pos += " " + str(block)
            print pos

    def clear_stack(self, stack, a):
        block = stack[-1]
        while block != a:
            stack.pop()
            self.world[block].append(block)
            block = stack[-1]

    def make_move(self, move, block1, block2):
        legal_move = True
        if block1 != block2:
            for stack in self.world:
                if set([block1, block2]).issubset(stack):
                    legal_move = False
                    break
            if legal_move:
                move(self, block1, block2)


def move_onto(world, a, b):
    for stack in world.world:
            if a in stack:
                world.clear_stack(stack, a)
                stack.pop()
    for stack in world.world:
            if b in stack:
                world.clear_stack(stack, b)
                stack.append(a)


def move_over(world, a, b):
    for stack in world.world:
        if a in stack:
            world.clear_stack(stack, a)
            stack.pop()
    for stack in world.world:
        if b in stack:
            stack.append(a)


def pile_onto(world, a, b):
    to_move = []
    for i, stack in enumerate(world.world):
        if a in stack:
            pos = stack.index(a)
            to_move = stack[pos:]
            world.world[i] = stack[:pos]
    for stack in world.world:
        if b in stack and to_move:
            world.clear_stack(stack, b)
            stack.extend(to_move)


def pile_over(world, a, b):
    to_move = []
    for i, stack in enumerate(world.world):
        if a in stack:
            pos = stack.index(a)
            to_move = stack[pos:]
            world.world[i] = stack[:pos]
    for stack in world.world:
        if b in stack and to_move:
            stack.extend(to_move)


if __name__ == '__main__':
    print "---------------\nPhase 1:"
    w = World(8)
    w.make_move(lambda *args: move_onto(*args), 7, 1)
    w.make_move(lambda *args: move_onto(*args), 5, 1)
    w.make_move(lambda *args: move_onto(*args), 1, 6)
    w.make_move(lambda *args: move_onto(*args), 4, 3)
    w.make_move(lambda *args: move_onto(*args), 1, 4)
    w.make_move(lambda *args: move_onto(*args), 3, 1)
    w.make_move(lambda *args: move_onto(*args), 5, 2)
    w.make_move(lambda *args: move_onto(*args), 7, 5)
    w.make_move(lambda *args: move_onto(*args), 4, 5)
    assert w.world == [[0], [1], [2, 5, 4], [3], [], [], [6], [7]]
    w.print_world()
    # -------------------------------
    print "---------------\nPhase 2:"
    w = World(6)
    w.make_move(lambda *args: move_onto(*args), 4, 1)
    w.make_move(lambda *args: move_onto(*args), 3, 2)
    w.make_move(lambda *args: move_over(*args), 2, 1)
    w.make_move(lambda *args: move_onto(*args), 4, 2)
    w.make_move(lambda *args: move_over(*args), 5, 0)
    w.make_move(lambda *args: move_over(*args), 4, 0)
    assert w.world == [[0, 5, 4], [1], [2], [3], [], []]
    w.print_world()
    # -------------------------------
    print "---------------\nPhase 3:"
    w = World(10)
    w.make_move(lambda *args: move_onto(*args), 9, 1)
    w.make_move(lambda *args: move_over(*args), 8, 1)
    w.make_move(lambda *args: move_over(*args), 7, 1)
    w.make_move(lambda *args: move_over(*args), 6, 1)
    w.make_move(lambda *args: pile_onto(*args), 8, 6)
    w.make_move(lambda *args: pile_onto(*args), 8, 5)
    w.make_move(lambda *args: move_over(*args), 2, 1)
    w.make_move(lambda *args: move_over(*args), 4, 9)
    w.make_move(lambda *args: pile_onto(*args), 8, 9)
    assert w.world == [[0], [1, 9, 8, 7, 6], [2], [3], [4], [5], [], [], [], []]
    w.print_world()
    # -------------------------------
    print "---------------\nPhase 4:"
    w = World(10)
    w.make_move(lambda *args: move_onto(*args), 9, 1)
    w.make_move(lambda *args: move_over(*args), 8, 1)
    w.make_move(lambda *args: move_over(*args), 7, 1)
    w.make_move(lambda *args: move_over(*args), 6, 1)
    w.make_move(lambda *args: pile_over(*args), 8, 6)
    w.make_move(lambda *args: pile_over(*args), 8, 5)
    w.make_move(lambda *args: move_over(*args), 2, 1)
    w.make_move(lambda *args: move_over(*args), 4, 9)
    assert w.world == [[0], [1, 9, 2, 4], [], [3], [], [5, 8, 7, 6], [], [], [], []]
    w.print_world()
